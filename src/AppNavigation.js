import { TabNavigator, StackNavigator } from 'react-navigation'
import { Dimensions, Image, View } from 'react-native'
import Colors from './components/const/colors'
// Tab
import Home from './components/tab/Home'
import Summary from './components/tab/Summary'
import Partner from './components/tab/Partner'
import HomePartner from './components/tab/HomePartner'
import SummaryPartner from './components/tab/SummaryPartner'
import Profile from './components/tab/Profile'
// Stack
import Initial from './components/stack/Initial'
import Login from './components/stack/Login'
import AddPartner from './components/stack/AddPartner'
import Generate from './components/stack/Generate'
import AddVoucher from './components/stack/AddVoucher'
import AddPromo from './components/stack/AddPromo'
import DetailSummary from './components/stack/DetailSummary'
const { width:viewPortWidth, height: viewPortHeight} = Dimensions.get('window')

  const primaryHeader = {
    backgroundColor: Colors.primary,
    elevation: 0,
    borderBottomWidth: 0,
  }

  const whiteHeader = {
    backgroundColor: Colors.white,
    elevation: 0,
    borderBottomWidth: 0,
  }

  const transHeader = {
    backgroundColor: 'transparent',
    elevation: 0,
    borderBottomWidth: 0,
  }

  const TabScreenPartner = TabNavigator({
    HomePartner: { screen: HomePartner },
    SummaryPartner: { screen: SummaryPartner },
    Profile: { screen: Profile }
  },
  { 
    initialRouteName: 'HomePartner',
    swipeEnabled: true,
    tabBarPosition: 'top',
    animationEnabled:false,
    tabBarOptions: 
    {
      activeTintColor: Colors.primary,
      inactiveTintColor: 'rgba(0, 0, 0, 0.6)',
      tabStyle: { 
        height: 50,
        padding:5
      //  paddingBottom: 0, 
      },
      indicatorStyle: {
        borderBottomColor: '#ffffff',
        borderBottomWidth: 2,
        backgroundColor:'#fff'
      },
      style: {
        borderTopWidth:0.5,
        // borderBottomWidth:1,
        // borderBottomColor:'rgba(0,0,0,0.2)',
        backgroundColor: Colors.white
      },
      showLabel:false,
      showIcon: true,
    },
  });

  const TabScreenNavigator = TabNavigator({
    Home:    { screen: Home }, 
    Summary: { screen: Summary },
    Partner: { screen: Partner },
  }, 
  { 
    initialRouteName: 'Home',
    swipeEnabled: false,
    tabBarPosition: 'top',
    animationEnabled:false,
    tabBarOptions: 
    {
      activeTintColor: Colors.primary,
      inactiveTintColor: 'rgba(0, 0, 0, 0.6)',
      tabStyle: { 
        height: 50,
        padding:5
      //  paddingBottom: 0, 
      },
      indicatorStyle: {
        borderBottomColor: '#ffffff',
        borderBottomWidth: 2,
        backgroundColor:'#fff'
      },
      style: {
        borderTopWidth:0.5,
        // borderBottomWidth:1,
        // borderBottomColor:'rgba(0,0,0,0.2)',
        backgroundColor: Colors.white
      },
      showLabel:false,
      showIcon: true,
    },
  });

  const AppNavigation = StackNavigator({
    Tab:              { screen: TabScreenNavigator },
    TabPartner:       { screen: TabScreenPartner },
    Initial:          { screen: Initial },
    Login:            { screen: Login },
    AddPartner:       { screen: AddPartner },
    Generate:         { screen: Generate },
    AddVoucher:       { screen: AddVoucher },
    AddPromo:         { screen: AddPromo },
    DetailSummary:    { screen: DetailSummary },
  }, 
  {
    animationEnabled:false,
    initialRouteName: 'Initial'
  });

  const prevGetStateForActionHomeStack = AppNavigation.router.getStateForAction;
  AppNavigation.router.getStateForAction = (action, state) => {
      if (state && action.type === 'ReplaceCurrentScreen') {
        const routes = state.routes.slice(0, state.routes.length - 1);
        routes.push(action);
        return {
          ...state,
          routes,
          index: routes.length - 1,
        };
      }
      return prevGetStateForActionHomeStack(action, state);
  }

  AppNavigation.navigationOptions = {
    headerStyle: primaryHeader,
    headerTitleStyle :{width:viewPortWidth-80},
    headerTintColor: '#fff',
  };

  TabScreenNavigator.navigationOptions = {
    headerStyle: primaryHeader,
    headerTitleStyle :{textAlign: 'center',alignSelf:'center',width:viewPortWidth-80},
    headerTintColor: '#fff',
    title:'Giftamo'
  };
  TabScreenPartner.navigationOptions = {
    headerStyle: primaryHeader,
    headerTitleStyle :{textAlign: 'center',alignSelf:'center',width:viewPortWidth-80},
    headerTintColor: '#fff',
    title:'Giftamo'
  };

export default AppNavigation;