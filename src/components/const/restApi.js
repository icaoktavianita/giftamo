// const baseUrl = 'http://idphotobook.com:3000/';http://103.89.3.246:8010/
const production = true
const baseUrl = production ? 'http://103.89.3.246:8010/' : 'http://139.59.245.193:8000/v1/';
const baseUrlShare = 'http://papito.inspiralocal.id/vouchers/';

export default {
  login:baseUrl+'users/login',
  //giftamo
  voucher: baseUrl+'vouchers/', //get,post(jumlah)
  summaryVoucher: baseUrl+'vouchers/summary',
  summaryPartner: baseUrl+'vouchers/summarypartner',
  partner: baseUrl+'partners/',
  usedVoucher: baseUrl+'vouchers/used',//put(id)
  promo: baseUrl+'promos/',
  user: baseUrl+'users/',
  share: baseUrlShare
  

}