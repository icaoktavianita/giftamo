import React from 'react'
import {
    View,
    Button,
    StyleSheet,
    Dimensions,
    DeviceEventEmitter,
} from 'react-native'
import API from '../const/restApi'
import { post } from '../services/rest'
import Colors from '../const/colors'
import AccordionPicker from '../common/AccordionPicker'
import moment from 'moment/min/moment-with-locales'
import * as t from 'tcomb-form-native'
import * as _ from 'lodash'

const { width, height } = Dimensions.get('window')

// clone the default stylesheet
const stylesheet = _.cloneDeep(t.form.Form.stylesheet);
stylesheet.textbox.normal.margin = 10;
stylesheet.textbox.normal.marginTop = 5;
stylesheet.textbox.normal.height = 45;
stylesheet.textbox.error.margin = 10;
stylesheet.textbox.error.marginTop = 5;
stylesheet.textbox.error.height = 45;

stylesheet.errorBlock.margin = 15;
stylesheet.errorBlock.marginTop = 0;

const addPromo = t.struct({
    promo:t.String,
    expired_date:t.Date
  });
  const myFormatFunction =(format,date) => {
      return moment(date).format(format);
  }
  const options = {
    auto:'none',
    fields: {
      promo: {
        placeholder: 'Promo Name',
        error: 'Enter your promo name',
        stylesheet: stylesheet,
      },
      expired_date: {
        label: 'Expired date',
        mode: 'date',
        config:{
            format:(date) => myFormatFunction('YYYY-MM-DD',date)
        }
      }
    }
  };
  const Form = t.form.Form;
class AddPromo extends React.Component {
    static navigationOptions = {
        title:'Add Promo'
    }
    submit(){
        let value = this.refs.form.getValue();
        if (value){
            let params = {
                "promo": value.promo,
                "expired_date": moment(value.expired_date).format('YYYY-MM-DD').toString()
            }
            post(API.promo,params).then(res=>{
                alert('Success')
                this.props.navigation.goBack()
                DeviceEventEmitter.emit('AddPromo', res.data)
            }).catch(err=>{
                alert('gagal')
                console.log(err)
            })
        }
    }
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.form}>
                    <Form
                        ref="form"
                        type={addPromo}
                        options={options}
                    />
                    <Button 
                        onPress={()=>this.submit()}
                        medium
                        buttonStyle={styles.button}
                        title='Submit'
                    />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1, 
        backgroundColor:Colors.white,
    },
    form:{
        flex:0,
        padding: 45,
    },
    button:{
        marginTop:5,
        borderRadius:5,
        backgroundColor:Colors.primary
    },
})

export default AddPromo