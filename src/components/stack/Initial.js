import React from 'react'
import {isSignedIn} from '../const/auth'
import { NavigationActions } from 'react-navigation'

import { Platform } from 'react-native'
class Initial extends React.Component{
    static navigationOptions = ({ navigation }) => ({
     header:null
    });
    constructor(props){
        super(props)
    }

    componentWillMount(){  
     isSignedIn().then(res=>{
         this.navigate(res ? (res == 1 ? 'Tab':'TabPartner') : 'Login')
     })
    }
    navigate(routeName){
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName})]
        })
        this.props.navigation.dispatch(resetAction)
    }
    render(){
     return null
    }
}
    export default Initial