import React from 'react'
import {
    View,
    Image,
    Text,
    Alert,
    TextInput,
    Platform,
    Dimensions,
    ActivityIndicator,
    AsyncStorage,
    AppRegistry,
    DeviceEventEmitter,
    StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/dist/Feather'
import { Avatar, FormLabel, FormInput, Button } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Permissions from 'react-native-permissions'
import RNFetchBlob from 'react-native-fetch-blob'
import { upload,post } from '../services/rest'
import Colors from '../const/colors'
import API from '../const/restApi'
import * as t from 'tcomb-form-native'
import * as _ from 'lodash'

const { width, height } = Dimensions.get('window')

// clone the default stylesheet
const stylesheet = _.cloneDeep(t.form.Form.stylesheet);
stylesheet.textbox.normal.margin = 10;
stylesheet.textbox.normal.marginTop = 5;
stylesheet.textbox.normal.height = 45;
stylesheet.textbox.error.margin = 10;
stylesheet.textbox.error.marginTop = 5;
stylesheet.textbox.error.height = 45;

stylesheet.errorBlock.margin = 15;
stylesheet.errorBlock.marginTop = 0;
const Email = t.refinement(t.String, (n) => {
    let reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return reg.test(n)
    });
  Email.getValidationErrorMessage = (value, path, context) => {
    return value ? 'email tidak valid':'masukkan email anda'
  };
  var min6 = s => s.length >= 6;
  var passwordVal = t.refinement(t.String, s=> min6(s));
  passwordVal.getValidationErrorMessage = s => {
    if(!s){
        return 'We need a password to proceed!';
    }
    if(!min6(s)){
        return (
            alert('Password should be 6 character')
        )
    }
};
  const add = t.struct({
    full_name:t.String,
    email:Email,
    password:passwordVal,
    phone: t.String
  });
  const options = {
    auto:'none',
    fields: {
      full_name: {
        placeholder: 'Full Name',
        error: 'Enter your full name',
        stylesheet: stylesheet,
      },
      phone: {
        placeholder: 'Phone',
        error: 'Enter your Phone',
        stylesheet: stylesheet,
    },
      email: {
        placeholder: 'Email',
        error: 'Enter your email',
        stylesheet: stylesheet,
      },
      password: {
        placeholder: 'Password',
        secureTextEntry:true,
        error: 'Enter your password',
        stylesheet: stylesheet
      },
    }
  };

  const Form = t.form.Form;
const isEmpty = (value) => value === undefined || value === '' || value === null || (typeof value === "object" && Object.keys(value).length === 0) || (typeof value === "string" && value.trim().length === 0)

class AddPartner extends React.Component {
  static navigationOptions = {
    tabBarIcon:({tintColor}) =>(
        <Icon name='map-pin' fontWeight={'bold'} size={22} color={tintColor} />
    ),
    title:'Add Partner'

};
  constructor(props){
    super(props)
    this.state = {
      uploadingImage: false,
      value: null,
    }
  }
  submit(){
    let value = this.refs.form.getValue();
    if (value){
        post(API.user,value).then(res=>{
            alert('Success')
            this.clearForm()
            DeviceEventEmitter.emit('AddPartner', res.data)
        }).catch(err=>{
            this.setState({loading:false})
        })
    }
}
clearForm(){
    this.setState({ value: null})
}
  render() {

    return (
        <View style={styles.container}>
            <KeyboardAwareScrollView>
                <View style={styles.form}>
                    <Form
                        ref="form"
                        type={add}
                        options={options}
                        value={this.state.value}
                    />
                    <Button 
                        onPress={()=>this.submit()}
                        medium
                        buttonStyle={styles.button}
                        title='Submit'
                    />
                </View>
            </KeyboardAwareScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
      flex:1, 
      backgroundColor:Colors.white,
  },
  headerView:{
      justifyContent: 'center',
      alignItems:'center',
      flex:40,
  },
  listView:{
      flex:60
  },
  photo:{
      width:width,
      height:'65%',
      // borderRadius:5,
      // borderColor:Colors.primary,
      // borderWidth:5,
  },
  editImage:{
      backgroundColor:'rgba(255,255,255,0.5)',
      maxWidth:40,
      height:40,
      justifyContent:'center',
      alignItems:'center',
      borderRadius:20,
      position:'absolute',
      width:width/2,
      zIndex:1,
  },
  uploadingImage:{
      maxWidth:40,
      height:40,
      justifyContent:'center',
      borderRadius:20,
      position:'absolute',
      width:width/2,
      zIndex:1,
      alignItems:'center'
  },
  form:{
    flex:0,
    padding: 45,
},
button:{
    marginTop:5,
    borderRadius:5,
    backgroundColor:Colors.primary
},
})

export default AddPartner