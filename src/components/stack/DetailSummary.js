import React from 'react'
import {
    Text,
    View,
    FlatList,
} from 'react-native'
import { post } from '../services/rest'
import API from '../const/restApi'
import moment from 'moment'
import { ListItem } from 'react-native-elements';
import Colors from '../const/colors'

class DetailSummary extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            date: '',
            data:[],
        }
    }
    componentWillMount(){
        var url = this.props.navigation.state.params.url
        this.setState({date:  url});
      }
    componentDidMount(){
        this.getData()
    }
    getData(){
        let tanggal = this.state.date
        console.log('tanggal', tanggal)
        post(API.voucher+'detail/'+tanggal)
        .then(res=>{
            this.setState({data: res.data})
        })
        .catch(err=>{
            console.log(err)
        })
    }
    renderItem(item,index){
        return(
            <View>
                <ListItem
                    containerStyle={{backgroundColor:'#fff'}}
                    title={item.item.full_name}
                    titleStyle={{fontSize:18,color:Colors.dark}}
                    subtitle={item.item.jumlah}
                    subtitleStyle={{color:Colors.dark}}
                    hideChevron={true}
                />
                
            </View>
        )
    }
    render(){
        return (
            <View>
                <FlatList
                    extraData={this.state}
                    data={this.state.data}
                    keyExtractor={(item,key)=>key}
                    renderItem={(item,index)=>this.renderItem(item,index)}
                />
            </View>
        )
    }
}

export default DetailSummary