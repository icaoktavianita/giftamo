import React from 'react'
import { 
    View,
    Image,
    Text,
    TextInput,
    Platform,
    ActivityIndicator,
    AsyncStorage,
    StyleSheet} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { NavigationActions } from 'react-navigation'
import { FormLabel,  FormInput,Button, SocialIcon} from 'react-native-elements'
import CONFIG from '../const/config'
import API from '../const/restApi'
import socialColors from '../const/socialColors'
import Colors from '../const/colors'
import {post} from '../services/rest'
import { onSignIn, isSignedIn } from '../const/auth'
import * as t from 'tcomb-form-native'
import * as _ from 'lodash'

// clone the default stylesheet
const stylesheet = _.cloneDeep(t.form.Form.stylesheet);
stylesheet.textbox.normal.margin = 15;
stylesheet.textbox.normal.marginTop = 5;
stylesheet.textbox.normal.height = 45;
stylesheet.textbox.error.margin = 15;
stylesheet.textbox.error.marginTop = 5;
stylesheet.textbox.error.height = 45;

stylesheet.errorBlock.margin = 15;
stylesheet.errorBlock.marginTop = 0;
const Email = t.refinement(t.String, (n) => {
    let reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return reg.test(n)
    });
  Email.getValidationErrorMessage = (value, path, context) => {
    return value ? 'email tidak valid':'masukkan email anda'
  };
const user = t.struct({
    email:t.String,
    password:t.String
  });
  const options = {
    auto:'none',
    fields: {
      email: {
        placeholder: 'Email',
        stylesheet: stylesheet,
        error: 'Enter your email'
      },
      password: {
        placeholder: 'Password',
        secureTextEntry:true,
        error: 'Enter your password',
        stylesheet: stylesheet
      },
    }
  };
  const Form = t.form.Form;
const isEmpty = (value) => value === undefined || value === '' || value === null || (typeof value === "object" && Object.keys(value).length === 0) || (typeof value === "string" && value.trim().length === 0)

class Login extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            loading: false,
            token:''
        }
    }
    
    next(){
        this.props.navigation.goBack()    
    }
    ToTab(){
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Initial'})]
        })
        this.props.navigation.dispatch(resetAction)
    }
    render(){
        const animating = this.state.loading

        if(this.state.loading){
            return(
                <ActivityIndicator animating={animating} color='#3498db'
                size='small'
                style={style.loading}/>
            )
        }
        return (
            <View style={style.container}>
            <KeyboardAwareScrollView>
                <Text style={style.title}>Giftamo</Text>
                
                <View style={style.form}>
                <Form
                ref="form"
                type={user}
                options={options}
                />

                <Button 
                onPress={()=>this.login()}
                medium
                buttonStyle={style.button}
                title='Login'/>

                </View>
            </KeyboardAwareScrollView>
            </View>

        )
    }
    login(){
        const { navigate,state } = this.props.navigation;
        let value = this.refs.form.getValue();
        if (value) { 
            this.setState({loading:true})
            post(API.login,value).then(res=>{
                res.data.token = res.meta.token
                onSignIn(JSON.stringify(res.data))
                this.setState({loading:false})
                this.ToTab()
            }).catch(err=>{
                this.setState({loading:false})
                alert('Username or password is wrong')
            })
        }
        
    }
}

const style = StyleSheet.create({
    loading:{
        flex:1,
    },
    socialButton: {
        marginLeft:30,
        marginRight:30
    },
    button:{
        marginTop:15,
        borderRadius:5,
        backgroundColor:Colors.primary
    },
    buttonClear:{
        marginTop:20,
        borderRadius:5,
    },
    labelContainerStyle: {
        fontSize:30,
        marginTop: 8,
    },
    container:{
        flex:1, 
        backgroundColor:'#fff',
        justifyContent:'center'
    },
    icon:{
        marginBottom:20,
        alignItems:'center'
    },
    logo:{
        width:80,
        height:80,
        margin:'auto',
        alignItems:'center'
    },
    title:{
        color:'#333',
        fontWeight:'100',
        fontSize:25,
        marginBottom:50,
        textAlign:'center'
    },
    subtitle:{
        color:'#d3d3d3',
        fontSize:15,
        margin:20,
        textAlign:'center'
    },
    grid: {
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        flex: 0,
    },
    gridItem: {
        margin:5,
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
    },
    form:{
        flex:0,
        paddingLeft:45,
        paddingRight:45
    },
    
})

    
    export default Login