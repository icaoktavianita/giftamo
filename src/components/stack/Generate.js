import React, { Component } from 'react'
import QRCode from 'react-native-qrcode'
import { Button } from 'react-native-elements'
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    DeviceEventEmitter,
} from 'react-native'
import Share, { ShareSheet } from 'react-native-share'
import API from '../const/restApi'
import { put } from '../services/rest'
import Icon from 'react-native-vector-icons/dist/Feather'

class Home extends React.Component{
  static navigationOptions = {
    title: 'QR Code Preview'
}
    state = {
        visible: false,
        id: ''
      }
    componentWillMount(){
      this.setState({id: this.props.navigation.state.params.url });
    }
    onOpen(){
       this.setState({ visible: true })
     }
    onCancel(){
       this.setState({ visible: false })
     }
     shared(item){
      let id = item;
      let value = {"id":id}
      let shareOptions = {
          title: "React Native",
          message: "This is your voucher",
          url: API.share+id,
        }
    
        let shareImageBase64 = {
          title: "React Native",
          message: "This is your voucher",
          url: API.share+id,
          subject: "Voucher" //  for email
        };
      Share.open(shareImageBase64)
      this.putIsShared(item)
  }
  onFinish(){
    this.props.navigation.goBack()
  }
  putIsShared(item){
      let id = item;
      let value = {"id":id}
      put(API.voucher+'shared/',value)
      .then(res=>{
          if(res){
            this.props.navigation.goBack()
            //DeviceEventEmitter.emit('Generate', res.data)   
          }
      }).catch(err=>{
          this.setState({loading:false})
          console.log(err)
      })
  }
      render() {
        let item = this.state.id
        return (
          <View style={styles.container}>
            <QRCode
              value={item}
            />
            <TouchableOpacity onPress={()=> {this.shared(item)}}>
              <View>
                <Text>SHARE</Text>
              </View>
            </TouchableOpacity>
          </View>
        );
      };
    }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {  
        borderWidth: 1,
        margin: 30,
        borderRadius: 5,
        padding: 5,
        backgroundColor: 'black'
    }
});

export default Home