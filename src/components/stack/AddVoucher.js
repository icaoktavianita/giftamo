import React from 'react'
import {
    View,
    Text,
    Alert,
    Button,
    FlatList,
    StyleSheet,
    TouchableOpacity,
    RefreshControl,
    DeviceEventEmitter,
} from 'react-native'
import Icon from 'react-native-vector-icons/dist/Feather'
import Colors from '../const/colors'
import API from '../const/restApi'
import {post, get} from '../services/rest'
import {Spinner} from '../common/Spinner'
import * as t from 'tcomb-form-native'
import * as _ from 'lodash'

const stylesheet = _.cloneDeep(t.form.Form.stylesheet);
//stylesheet.textbox.normal.margin = 15;
stylesheet.textbox.normal.marginTop = 5;
stylesheet.textbox.normal.height = 45;
//stylesheet.textbox.error.margin = 15;
stylesheet.textbox.error.marginTop = 5;
stylesheet.textbox.error.height = 45;

stylesheet.errorBlock.margin = 15;
stylesheet.errorBlock.marginTop = 0;

const voucher = t.struct({
    jumlah:t.Number,
  });
  const options = {
    auto:'none',
    fields: {
      jumlah: {
        placeholder: 'Enter the amount',
        stylesheet: stylesheet,
        error: 'Enter the right amount'
      },
    }
  };
  const Form = t.form.Form;

class AddVoucher extends React.Component {
    static navigationOptions = {
        title:'Voucher'
    
    };
    constructor(props){
        super(props)
        this.state = {
            data: [],            
            visible: false,
            refreshing:false,
            id_promo: '',
            loading: false

        }
    }
    componentWillMount(){
        // DeviceEventEmitter.addListener('Generate', (e)=>{
        //     this.setState({data:this.state.data.concat(e)})
        //     })
        this.setState({id_promo: this.props.navigation.state.params.url });
      }
    componentDidMount(){
        this._getData()
    }
    _getData(){
        var id = this.state.id_promo
        this.setState({loading: true, refreshing: true})
        return new Promise((resolve,reject)=>{
            post(API.voucher+id).then(res=>{
                this.setState({data:res.data, refreshing: false,loading:false})
                resolve()
            }).catch(err=>{
                this.setState({refreshing:false,loading:false})
                reject()
            })
        })
    }
    refreshData(){
        this.setState({refreshing:true})
        this._getData()
        .then(()=>{
            this.setState({refreshing:false})
        }).catch(()=>{
            this.setState({refreshing:false})
        })
    }
    _onPress(){
        let value = this.refs.form.getValue();
        var id = this.state.id_promo
        if (value) {
            let params = {
                "jumlah": value.jumlah,
                "promo_id": id
            }
            post(API.voucher,params).then(res=>{
                if(res){
                    Alert.alert('Success')
                    this.refreshData()
                }
            }).catch(err=>{
                console.log(err)
            })
        }
    }
    renderItem({item,index}){
        return(
            <View style={style.qrcode}>
              <TouchableOpacity onPress={()=> {this.props.navigation.navigate('Generate', { url: item.id })}}>
                <Text>{index+1} {API.share+item.id}</Text>
              </TouchableOpacity>
            </View>
        )
    }
    render(){
        return (
            <View style={style.container}>
                <View style={style.form}>
                    <Form
                        ref="form"
                        type={voucher}
                        options={options}
                    />
                    <Button 
                        onPress={()=>this._onPress()}
                        small
                        buttonStyle={style.button}
                        title='Generate'
                    />
                </View>
                <Text>Total = {this.state.data.length}</Text>
                <FlatList
                    extraData={this.state}
                    data={this.state.data}
                    keyExtractor={(item,key)=>key}
                    renderItem={(item,index)=>this.renderItem(item,index)}
                    refreshControl={
                        <RefreshControl
                            color={Colors.white}
                            refreshing={this.state.refreshing}
                            onRefresh={()=>this.refreshData()}
                        />
                    }
                />
            </View>
        )
    }
}

const style = StyleSheet.create({
    button:{
        marginTop:15,
        borderRadius:5,
        backgroundColor:Colors.primary
    },
    buttonClear:{
        marginTop:20,
        borderRadius:5,
    },
    labelContainerStyle: {
        fontSize:30,
        marginTop: 8,
    },
    container:{
        flex:1, 
        backgroundColor:'#fff',
    },
    icon:{
        marginBottom:20,
        alignItems:'center'
    },
    logo:{
        width:80,
        height:80,
        margin:'auto',
        alignItems:'center'
    },
    title:{
        color:'#333',
        fontWeight:'100',
        fontSize:25,
        marginBottom:50,
        textAlign:'center'
    },
    subtitle:{
        color:'#d3d3d3',
        fontSize:15,
        margin:20,
        textAlign:'center'
    },
    grid: {
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        flex: 0,
    },
    gridItem: {
        margin:5,
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
    },
    form:{
        flex:0,
        padding:5,
    },
    qrcode: {
        flex: 1,
        margin: 10,
    }
    
})

export default AddVoucher