import React from 'react';
import { View, ActivityIndicator, Text } from 'react-native';
import Colors from '../const/colors';

function renderText(val){
  if(val){
    return (
      <Text style={{margin:10}}>{val}</Text>
    )
  }
}
const Spinner = ({ size,text }) => {
  return (
    <View style={styles.spinnerStyle}>
    <View>
      <ActivityIndicator size={size || 'large'} />
      {renderText(text)}
      </View>
    </View>
  );
}

const styles = {
  spinnerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:Colors.clouds
  }
}

export { Spinner };