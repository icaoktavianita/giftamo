import { AsyncStorage } from "react-native";
import CONFIG from '../const/config';
import axios from 'axios';
import RNFetchBlob from 'react-native-fetch-blob';
function setHeaderToken(){
 return new Promise((resolve,reject)=>{
  AsyncStorage.getItem(CONFIG.AUTH).then(val=>{
   if(val){
    val = JSON.parse(val)
    axios.defaults.headers.common['Authorization'] = val.token;
    resolve(val.token)
   }else{
     resolve()
   }
  }).catch(err=>{
   reject()
  })

 })
}
export const post = (url,data) => {
  return new Promise((resolve,reject)=>{
    setHeaderToken().then(()=>{
      return axios.post(url,data)
    })
    .then(res=>{
      resolve(res.data)
    })
    .catch(err=>{
      reject(err)
    })
  })
};
export const get = (url) => {
  return new Promise((resolve, reject)=>{
    setHeaderToken().then(()=>{
      return axios.get(url)
    })
    .then(res=>{
      resolve(res.data)
    })
    .catch(err=>{
      reject(err)
    })
  })
};
export const put = (url,data) => {
  return new Promise((resolve,reject)=>{
    setHeaderToken().then(()=>{
      return axios.put(url,data)
    })
    .then(res=>{
      resolve(res.data)
    })
    .catch(err=>{
      reject(err)
    })
  })
};
export const del = (url) => {
  return new Promise((resolve,reject)=>{
    setHeaderToken().then(()=>{
      return axios.delete(url)
    })
    .then(res=>{
      resolve(res.data)
    })
    .catch(err=>{
      reject(err)
    })
  })
};
export const upload =(url,param) => {
  return new Promise((resolve,reject)=>{
      setHeaderToken()
      .then(token=>{
        return RNFetchBlob.fetch('POST', url, {Authorization :token,'Content-Type' : 'multipart/form-data'},param)
      })
      .then((res) => {
        console.log(res.json())
        resolve(res.json())
      })
      .catch((err) => {
        reject(err)
        // error handling ..
      })
    })
}