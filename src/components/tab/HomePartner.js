import React, { Component } from 'react';
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Alert,
  DeviceEventEmitter,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/Feather'
import Camera from 'react-native-camera';
import API from '../const/restApi'
import { put } from '../services/rest'

class HomePartner extends Component {
  static navigationOptions = {
    tabBarIcon:({tintColor}) =>(
        <Icon name='home' size={22} color={tintColor} />
    ),
};

  render() {
    return (
      <View style={styles.container}>
        <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
	  onBarCodeRead={this.onBarCodeRead.bind(this)}
          style={styles.preview}
          aspect={Camera.constants.Aspect.fill}>
        </Camera>
      </View>
    );
  }

  onBarCodeRead(e) {
    let id = e.data;
    let value = {"id":id};
    if (e.data){
      put(API.usedVoucher,value).then(res=>{
        if(res){
          Alert.alert('Voucher valid')   
          this.props.navigation.navigate('SummaryPartner')
          DeviceEventEmitter.emit('HomePartner', res.data)
        }
      }).catch(err=>{
        if(err){
          alert('Voucher not valid')
        }
      })
    }
  }
  // <Text style={styles.capture} onPress={this.takePicture.bind(this)}>[CAPTURE]</Text>
  // takePicture() {
  //   const options = {};
  //   //options.location = ...
  //   this.camera.capture({metadata: options})
  //     .then((data) => console.log(data))
  //     .catch(err => console.error(err));
  // }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  }
});

export default HomePartner