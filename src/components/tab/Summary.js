import React from 'react'
import { 
    View,
    FlatList,
    Text,
    Dimensions,
    RefreshControl,
    TouchableOpacity,
    StyleSheet,} from 'react-native'
import { NavigationActions } from 'react-navigation'
import { ListItem } from 'react-native-elements'
import Icon from 'react-native-vector-icons/dist/Feather'
import _ from 'lodash'
import CONFIG from '../const/config'
import API from '../const/restApi'
import Colors from '../const/colors'
import {Spinner} from '../common/Spinner'
import { post } from '../services/rest'
import Timeline from '../common/Timeline'
import socialColors from '../const/socialColors'

const { width, height } = Dimensions.get('window')
const isEmpty = (value) => value === undefined || value === null || (typeof value === "object" && Object.keys(value).length === 0) || (typeof value === "string" && value.trim().length === 0)

class Summary extends React.Component{
    static navigationOptions = {
        tabBarIcon:({tintColor}) =>(
            <Icon name='list' fontWeight={'bold'} size={22} color={tintColor} />
        ),
    };
    constructor(props){
        super(props)
        this.renderDetail = this.renderDetail.bind(this)
        this.state = {
            loading: false,
            data:[],
            listData: [],
            refreshing: false,
            user: '',
            visible: true,
        }
    }
    componentDidMount(){
        this.getInitialData()
    }
    getInitialData(){
        this.getDataTimeline()
        .then(res=>{
          this.setState({listData: res,refreshing:false})
        }).catch(err=>{
          this.setState({refreshing:false})
        })
    }
    getDataTimeline(){
        return new Promise((resolve,reject)=>{
          post(API.summaryVoucher)
          .then(res=>{resolve(res.data)})
          .catch(err=>{reject(err)})
        })
    }
    renderTime(rowData) {
      var timeWrapper = 
        {
            alignItems: 'flex-end',
            flexWrap: 'wrap',
            width:100
        }
      return (
        <View style={timeWrapper}>
        <View style={[styles.timeContainer, this.props.timeContainerStyle]}>
          <Text style={[styles.time, this.props.timeStyle]}>{rowData.your_date}</Text>
        </View>
        </View>
      )
    }
    renderDetail(rowData) {
      return (
        <View style={{flex:1}}>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('DetailSummary', { url: rowData.your_date })}}>
                <Text style={[styles.title]}>{rowData.jumlah} vouchers is used</Text>
            </TouchableOpacity>
        </View>
      )
    }
    refreshData(){
        this.setState({refreshing:true})
        this.getDataTimeline().then(()=>{
            this.setState({refreshing:false})
        }).catch(()=>{
            this.setState({refreshing:false})
        })
    }
    render(){      
        if(this.state.loading){
            return(
                <Spinner/>
            )
        }
        return (
            <View style={styles.container}>
              <Timeline 
                  style={styles.list}
                  data={this.state.listData}
                  circleColor='rgb(45,156,219)'
                  lineColor='rgb(45,156,219)'
                  timeContainerStyle={{minWidth:52, marginTop: 0}}
                  timeStyle={{textAlign:'center', color:Colors.asbestos}}
                  descriptionStyle={{color:'white'}}
                  options={{enableEmptySections:true}}
                  renderDetail={this.renderDetail}
                  renderTime={this.renderTime}
                  refreshControl={
                    <RefreshControl
                      color={Colors.white}
                      refreshing={this.state.refreshing}
                      onRefresh={this.refreshData}
                    />
                  }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1, 
        backgroundColor:'#ffffff',
        justifyContent:'center'
    },
    
      button:{
          alignItems: 'flex-start',
          borderColor:Colors.dark,
          borderWidth:1,
          height:35,
          padding:10,
          borderRadius:5,
          justifyContent: 'center',
          alignSelf: 'flex-start',
      },
      list: {
        marginRight: 5
      },
      image:{
          marginTop: 10,
          width: '100%',
          height: 100,
      },
      descriptionContainer:{
        marginTop:10,
        marginRight:10
      },
      textDescription:{
        fontSize:14,
        color:Colors.dark
      }
})
    
    export default Summary