import React,{Component} from 'react'
import { 
  View,
  Text,
  Image,
  Alert,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  StyleSheet,
  Dimensions,
  DeviceEventEmitter} from 'react-native'
import { Button, Avatar } from 'react-native-elements'
import { NavigationActions } from 'react-navigation'
import Permissions from 'react-native-permissions'
import ImagePicker from 'react-native-image-picker'
import t from 'tcomb-form-native'
import _ from 'lodash'
import Icon from 'react-native-vector-icons/Feather'
import RNFetchBlob from 'react-native-fetch-blob'
import { upload,post } from '../services/rest'
import Colors from '../const/colors'
import API from '../const/restApi'
import CONFIG from '../const/config'
import { Spinner } from '../common/Spinner'

const { width:viewPortWidth, height:viewPortHeight } = Dimensions.get('window')

const optionsImagePicker = {
  mediaType:'photo',
 title: 'Pick Image',
 takePhotoButtonTitle:'Take Photo',
 cancelButtonTitle:'Cancel',
 chooseFromLibraryButtonTitle:'Open Gallery',
 storageOptions: {
   skipBackup: true,
 }
};

class Profile extends Component{
    static navigationOptions = {
        tabBarIcon:({tintColor}) =>(
            <Icon name='user' fontWeight={'bold'} size={22} color={tintColor} />
        ),
    };
  constructor(props){
      super(props)
      this.state = {
        loading:false,
        imageData:[],
        image:null,
        user: '',
      }
  } 
  componentWillMount(){
    this.setState({loading: true})
    AsyncStorage.getItem(CONFIG.AUTH).then(val=>{
            val = JSON.parse(val)
            this.setState({loading: false,user:val})
    })
  }  
  pickImage(){
    Permissions.checkMultiple(['camera', 'photo']).then(()=>{
      ImagePicker.showImagePicker(optionsImagePicker, (response) => {
       if(!response.didCancel) {
         let param = [{ 
                 name : 'file', 
                 filename : response.fileName, 
                 data: RNFetchBlob.wrap(response.path),
                 // data:param.uri
             }]
             let source = { uri: `data:image/jpeg;base64,${response.data}` };
             this.setState({image:source, imageData:param})
       }
       else if (response.error) {
         console.log('ImagePicker Error: ', response.error);
       }
      });
    })
  }
  deleteImage(){
    this.setState({image:null,imageData:[]})
  }
  
  uploadImage(param){
    if(param){
      upload(API.user+'upload',param).then(res=>{
        var uriImage = {uri: res.data.photo}
        this.setState({loading:false, image: uriImage, user: res.data})
        alert('Success')
      })
      .catch(err=>{
        this.setState({loading:false})
      })
    }
  }
  submit(){
    if(this.state.image){
        let param = this.state.imageData
        param.push()
        this.uploadImage(param)
      }else{
        this.setState({loading:false})
      }
  }
  confirm(){
    AsyncStorage.removeItem(CONFIG.AUTH).then(()=>{
        this.setState({loading: false,user:''})
            const resetAction = NavigationActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Login'})]
            })
            this.props.navigation.dispatch(resetAction)
    })
   }
  logout = () =>{
    Alert.alert(
        'Keluar Sekarang',
        'Apakah anda yakin?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => this.confirm()},
        ],
        { cancelable: false }
      )
    }
    confirmUpload(){
      Alert.alert(
        'You just have one chance to upload your photo',
        'Do you understand?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Yes', onPress: () => this.pickImage()},
        ],
        { cancelable: false }
      )
    }
  render(){
    if(this.state.loading){
      return <Spinner/>
    }
    return(
      <View style={styles.container}>
          <View style={styles.headerView}>
            {this.state.user.photo==='http://103.89.3.246:8010/uploads/users/null' ? 
            <View style={{justifyContent:'center',alignItems:'center'}}>
                <Avatar rounded source={this.state.image} width={120} height={120}/>  
                <View style={styles.editImage}><Icon reverse name='camera' color={Colors.white} size={20} onPress={()=>this.confirmUpload()}/></View>
                <Button 
                  onPress={()=>this.submit()}
                  medium
                  buttonStyle={styles.button}
                  title='Submit'
                />
            </View>
            :
            <View style={{justifyContent:'center',alignItems:'center'}}>
              <Avatar rounded source={{uri:this.state.user.photo}} width={120} height={120}/>
            </View>
            }
          </View>
          
          <View style={styles.form}>
            
          </View>
          <View style={styles.margin}>
                    <Button title='Logout' fontSize={17} backgroundColor={Colors.danger} buttonStyle={{borderRadius:5}} onPress={this.logout.bind(this)}/>
              </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button:{
      marginTop:15,
      borderRadius:5,
      backgroundColor:Colors.primary
  },
  container:{
      flex:1, 
      backgroundColor:'#fff',
      justifyContent:'center'
  },
  headerView:{
    justifyContent: 'center',
    alignItems:'center',
    flex:40
  },
  editImage:{
    backgroundColor:Colors.green,
    maxWidth:40,
    height:40,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:20,
    position:'absolute',
    width:viewPortWidth/2,
    zIndex:1,
  },
  form:{
      flex:60,
      paddingLeft:25,
      paddingRight:25
  },
  
})
export default Profile