import React from 'react'
import {
    Text,
    View,
    FlatList,
    RefreshControl,
    DeviceEventEmitter,
} from 'react-native'
import { post } from '../services/rest'
import API from '../const/restApi'
import Icon from 'react-native-vector-icons/dist/Feather'
import { ListItem } from 'react-native-elements';
import Colors from '../const/colors'
class SummaryPartner extends React.Component {
    static navigationOptions = {
        tabBarIcon:({tintColor}) =>(
            <Icon name='list' size={22} color={tintColor} />
        ),
    };
    constructor(props){
        super(props)
        this.state = {
            loading:true,
            data:[],
            refreshing: false
        }
    }
    componentWillMount() {
        DeviceEventEmitter.addListener('HomePartner', (e)=>{
            this.getData(e)
            })
        }
    componentDidMount(){
        this.getData()
    }
    getData(){
        this.setState({ loading: true })
        post(API.summaryPartner)
        .then(res=>{
            this.setState({data: res.data, loading: false})
        })
        .catch(err=>{
            console.log(err)
            this.setState({ loading: false })
        })
    }
    onRefresh(){
        this.setState({refreshing:true})
        this.getData().then(()=>{
            this.setState({refreshing:false})
        }).catch(()=>{
            this.setState({refreshing:false})
        })
    }
    renderItem(item,index){
        return(
            <View>
                <ListItem
                    containerStyle={{backgroundColor:'#fff'}}
                    title={item.item.your_date}
                    titleStyle={{fontSize:18,color:Colors.dark}}
                    subtitle={item.item.jumlah}
                    subtitleStyle={{color:Colors.dark}}
                    hideChevron={true}
                    refreshControl={
                        <RefreshControl
                          color={Colors.white}
                          refreshing={this.state.refreshing}
                          onRefresh={this.onRefresh.bind(this)}/>
                    }
                />
                
            </View>
        )
    }
    render(){
        return (
            <View>
                <FlatList
                    extraData={this.state}
                    data={this.state.data}
                    keyExtractor={(item,key)=>key}
                    renderItem={(item,index)=>this.renderItem(item,index)}
                />
            </View>
        )
    }
}

export default SummaryPartner