import React, { Component } from 'react'
import { ListItem } from 'react-native-elements'
import {
    DeviceEventEmitter,
    AsyncStorage,
    StyleSheet,
    RefreshControl,
    View,
    Button,
    Alert,
    Text,
    FlatList
} from 'react-native'
import Colors from '../const/colors'
import CONFIG from '../const/config'
import Icon from 'react-native-vector-icons/dist/Feather'
import { NavigationActions } from 'react-navigation'
import API from '../const/restApi'
import ActionButton from 'react-native-action-button'
import { get, del } from '../services/rest'
import Swipeout from 'react-native-swipeout'
import { Spinner } from '../common/Spinner'
import _ from 'lodash'

class Home extends Component {
  static navigationOptions = {
    tabBarIcon:({tintColor}) =>(
        <Icon name='home' size={22} color={tintColor} />
    ),
};
  constructor(props){
    super(props)
    this.state = {
        data: [],
        refreshing: false,
        loading: false
    };
  }
  componentWillMount() {
    DeviceEventEmitter.addListener('AddPromo', (e)=>{
        this.setState({data:this.state.data.concat(e)})
        })
    }
    componentDidMount(){
        this._loadInitialState()
    }

  _loadInitialState(){
    AsyncStorage.getItem(CONFIG.AUTH).then(val=>{
        if (val !== null){
            val = JSON.parse(val)
            this.setState({loading: true})
            this.getData().then(()=> {
              this.setState({loading:false})
            }).catch(()=>{
                this.setState({loading:false})
            })
        }
    })
  }
  getData(){
      return new Promise((resolve,reject)=>{
        get(API.promo).then(res=>{
            this.setState({data:res.data})
            resolve()
        }).catch(err=>{
            console.log('hhh',JSON.parse(JSON.stringify(err)))
            reject()
        })
      })
}

  confirm(){
    AsyncStorage.removeItem(CONFIG.AUTH).then(()=>{
        this.setState({loading: false,user:''})
            const resetAction = NavigationActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Login'})]
            })
            this.props.navigation.dispatch(resetAction)
    })
   }
  logout = () =>{
    Alert.alert(
        'Signout now?',
        'Are you sure?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => this.confirm()},
        ],
        { cancelable: false }
      )
    }
    confirmDelete(item,index){
        Alert.alert(
            'Delete Promo?',
            'Are you sure?',
            [
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'OK', onPress: () => this._delete(item,index)},
            ],
            { cancelable: false }
          )
    }
    _delete(item, index){
        del(API.promo+item.id)
        .then(res=>{
            this.setState((prevState) => ({
                data: prevState.data.filter((_, i) => i !== index)
            }));
        })
        .catch(err=>{
            console.log(err)
            
        })
    }
    renderItem({item,index}){
        let buttons = [
            {
                backgroundColor:Colors.danger,
                underlayColor:Colors.dangerActive,
                component:<View style={{justifyContent:'center',alignItems:'center',flex:1}}><Icon name='trash-2' color='#fff' size={20}/><Text style={{color:'#fff',fontSize:12}}>Hapus</Text></View>,
                onPress:()=>this.confirmDelete(item,index)
            },
        ]
        return (
            <Swipeout autoClose={true} right={buttons} backgroundColor='#c3bef9' style={{borderBottomWidth:1,borderBottomColor:'rgba(0,0,0,0.15)'}}>
                <ListItem
                    containerStyle={{borderBottomWidth:0}}
                    title={item.promo}
                    titleStyle={{fontSize:16, color:"#fff", fontWeight:"100"}}
                    subtitle={item.description}
                    subtitleStyle={{fontSize:13, color:Colors.asbestos}}
                    hideChevron={true}
                    onPress={() => {this.props.navigation.navigate('AddVoucher', { url: item.id })}}
                />
            </Swipeout>
        )
    }
    refreshData(){
        this.setState({refreshing:true})
        this.getData().then(()=>{
            this.setState({refreshing:false})
        }).catch(()=>{
            this.setState({refreshing:false})
        })
    }
      render() {
        const { navigate } = this.props.navigation;
            if(this.state.loading){
                return(<Spinner/>)
                }
        return (
          <View style={styles.container}>
            <FlatList
                refreshControl={
                    <RefreshControl
                        color={Colors.white}
                        refreshing={this.state.refreshing}
                        onRefresh={()=>this.refreshData()}
                    />
                }
                contentContainerStyle={styles.list}
                extraData={this.state}
                data={this.state.data}
                keyExtractor={(key,i)=>i}
                renderItem={(item,index)=>this.renderItem(item,index)}
            />
            <ActionButton style={{marginBottom: 10}} buttonColor="rgba(231,76,60,1)" onPress={() => {
                this.props.navigation.navigate('AddPromo')}}
            >
            </ActionButton>
            <View style={styles.margin}>
                <Button title='Logout' fontSize={17} backgroundColor={Colors.danger} buttonStyle={{borderRadius:5}} onPress={this.logout.bind(this)}/>
              </View>
           
          </View>
        );
      };
    }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    promos: {
        backgroundColor: '#c3bef9', 
        paddingRight: 140,
        paddingLeft: 140,
        paddingTop: 15,
        paddingBottom: 15,
        marginBottom: 5,
        alignItems:'center'
    },
});

export default Home