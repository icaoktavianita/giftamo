import React from 'react'
import {
    View,
    Text,
    FlatList,
    RefreshControl,
    Dimensions,
    DeviceEventEmitter,
    StyleSheet,} from 'react-native'
import navigationOptions from 'react-navigation'
import Icon from 'react-native-vector-icons/dist/Feather'
import ActionButton from 'react-native-action-button'
import { Avatar, FormLabel, FormInput, Button } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Colors from '../const/colors'
import API from '../const/restApi'
import { get } from '../services/rest'

const { width, height } = Dimensions.get('window')
const pict = '../assets/user.png';
class Track extends React.Component {
    static navigationOptions = {
        tabBarIcon:({tintColor}) =>(
            <Icon name='user' fontWeight={'bold'} size={22} color={tintColor} />
        ),
    };
  constructor(props){
    super(props)
    this.state = {
      uploadingImage: false,
      data: [],
      refreshing: false,
    }
  }
  componentWillMount() {
    DeviceEventEmitter.addListener('AddPartner', (e)=>{
        this.setState({data:this.state.data.concat(e)})
        })
    }
  getData(){
    return new Promise((resolve,reject)=>{
      get(API.partner).then(res=>{
          this.setState({data:res.data})
          resolve()
      }).catch(err=>{
          console.log(JSON.parse(JSON.stringify(err)))
          reject()
      })
    })
}
componentDidMount(){
  this.getData()
}
  renderItem({item, index}){
    let photo = item.photo
    return(
      <View>
      <View style={styles.headerView}>
        <View style={{ alignItems:'center', flex:35,marginBottom:10 }}>
            <Avatar width={50} height={50} rounded source={{uri: photo}} />
            <Text style={{fontSize:12}}>{item.full_name}</Text>
          </View>

          <View style={{flex:65 }}>
            <Text style={{marginBottom:5,fontSize:12}}>{item.email}</Text>
          </View>
        </View>
    </View>
    )
  }
  refreshData(){
    this.setState({refreshing:true})
    this.getData().then(()=>{
        this.setState({refreshing:false})
    }).catch(()=>{
        this.setState({refreshing:false})
    })
}
  render() {
    return (
      <View style={styles.container}>
        <FlatList
        contentContainerStyle={styles.list}
        extraData={this.state}
        data={this.state.data}
        keyExtractor={(key,i)=>i}
        renderItem={(item, index)=>this.renderItem(item, index)}
        refreshControl={
          <RefreshControl
              color={Colors.white}
              refreshing={this.state.refreshing}
              onRefresh={()=>this.refreshData()}
          />
      }
        />
        <ActionButton buttonColor="rgba(231,76,60,1)" onPress={() => {
                this.props.navigation.navigate('AddPartner')
            }}>
        </ActionButton>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container:{
      flex:1, 
      backgroundColor:Colors.white,
      justifyContent:'center'
  },
  headerView:{
      alignItems:'center',
      flex:10,
      flexDirection: 'row',
  },
  listView:{
      flex:60
  },
  list: {
    backgroundColor:Colors.clouds,
    margin:5,
    justifyContent: 'center',
    flexDirection: 'column',
},
  photo:{
      width:width,
      height:'65%',
      // borderRadius:5, 
      // borderColor:Colors.primary,
      // borderWidth:5,
  },
  headerView:{
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems:'center',
    flex:35,
    width: width
  },
  editImage:{
      backgroundColor:'rgba(255,255,255,0.5)',
      maxWidth:40,
      height:40,
      justifyContent:'center',
      alignItems:'center',
      borderRadius:20,
      position:'absolute',
      width:width/2,
      zIndex:1,
  },
  uploadingImage:{
      maxWidth:40,
      height:40,
      justifyContent:'center',
      borderRadius:20,
      position:'absolute',
      width:width/2,
      zIndex:1,
      alignItems:'center'
  },
})

export default Track